import json
import boto3
import logging
import zipfile
import os
import io
import re
import yaml
import requests
import stringcase
import traceback
import time
from iac_plugin_common import repo_util, scan_util, report_util

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)
code_pipeline = boto3.client('codepipeline')


def find_artifact(event):
    try:
        object_key = event['CodePipeline.job']['data']['inputArtifacts'][0]['location']['s3Location']['objectKey']
        bucket = event['CodePipeline.job']['data']['inputArtifacts'][0]['location']['s3Location']['bucketName']

        return bucket, object_key

    except KeyError as err:
        raise KeyError("Couldn't get S3 object!\n%s", err)


def _get_user_parameters(job_data):
    try:
        configuration = job_data['actionConfiguration']['configuration']

        if ('UserParameters' not in configuration):
            return
        user_parameters = configuration['UserParameters']
        decoded_parameters = json.loads(user_parameters)
    except Exception as e:
        # We're expecting the user parameters to be encoded as JSON
        # so we can pass multiple values. If the JSON can't be decoded
        # then fail the job with a helpful message.
        raise Exception('UserParameters could not be decoded as JSON')
    return decoded_parameters


def put_job_success(job, message):
    """Notify CodePipeline of a successful job

    Args:
        job: The CodePipeline job ID
        message: A message to be logged relating to the job status

    Raises:
        Exception: Any exception thrown by .put_job_success_result()

    """
    print(message)
    message = message[:2040]
    code_pipeline.put_job_success_result(
        jobId=job, executionDetails={"summary": message})


def put_job_failure(job, message):
    """Notify CodePipeline of a failed job

    Args:
        job: The CodePipeline job ID
        message: A message to be logged relating to the job status

    Raises:
        Exception: Any exception thrown by .put_job_failure_result()

    """
    print(message)
    code_pipeline.put_job_failure_result(
        jobId=job, failureDetails={'message': 'Prisma Cloud IaC Scan: \n' + message, 'type': 'JobFailed'})


def output_artifact(event, tmpl_errors, tmpl_violations):
    try:
        bucket = event['CodePipeline.job']['data']['outputArtifacts'][0]['location']['s3Location']['bucketName']
        object_key = event['CodePipeline.job']['data']['outputArtifacts'][0]['location']['s3Location']['objectKey']
        reportf = "/tmp/report.csv"
        report_util.generate_csv_report(reportf, tmpl_errors, tmpl_violations)
        logging.info("Scan report generated")
        s3 = boto3.client('s3')
        with open(reportf, "rb") as f:
            s3.upload_fileobj(f, bucket, object_key)
        return "s3://" + bucket + "/" + object_key
    except Exception as e:
        logging.warning("No report generated: " + str(e))
        return None


def read_requird_env(envs):
    for e in envs:
        if e in os.environ and os.environ[e]:
            return os.environ[e]
    raise Exception("environment variable(s) required: " + str(envs))


def dump_zip2tmp_repo(obj_zip):
    tmp_repo = '/tmp/repo'
    with io.BytesIO(obj_zip.get()['Body'].read()) as tf:
        # rewind the file
        tf.seek(0)
        # Read the file as a zipfile and process the members
        with zipfile.ZipFile(tf, mode='r') as zipf:
            zipf.extractall('/tmp/repo')
    return tmp_repo


def create_tmpl_zip(repo, tmpl_type):
    logging.info("Creating template ZIP from " + repo)
    tmpl_zip = "/tmp/iac_scan.zip"
    tmpl_zipped = 0
    with zipfile.ZipFile(tmpl_zip, "w", zipfile.ZIP_DEFLATED) as zipf:
        for root, _, files in os.walk(repo):
            if re.search(r"/\.", root):  # no hidden dir
                continue
            for file in files:
                if file.startswith("."):  # no hidden file
                    continue
                if re.search(r"(\.zip|buildspec|azure-pipelines)$", file):
                    continue
                if not tmpl_type == "tf":
                    if not re.search(r"\.(json|yaml|yml)$", file.lower()):
                        continue
                zipf.write(root + "/" + file, "." +
                           root[len(repo):] + "/" + file)
                tmpl_zipped += 1
    return tmpl_zipped, tmpl_zip


def lambda_handler(event, context):
    try:
        job_id = event['CodePipeline.job']['id']
        account_id = event['CodePipeline.job']['accountId']
        function_name = event['CodePipeline.job']['data']['actionConfiguration']['configuration']['FunctionName']
        stage_name = event['CodePipeline.job']['data']['inputArtifacts'][0]['name']
        print("jobID:", job_id)
        pcs = read_requird_env(
            ['prisma_cloud_api_url', 'Prisma_Cloud_API_URL']).rstrip('/')
        accesskey = read_requird_env(['prisma_cloud_access_key', 'Access_Key'])
        secretKey = read_requird_env(['prisma_cloud_secret_key', 'Secret_Key'])
        asset_name = read_requird_env(
            ['prisma_cloud_asset_name', 'Asset_Name'])
        if (asset_name is None):
            raise Exception("Please define Asset name in the lambda environment variables. " +
                            "Environment variable name: Asset_Name")
        job_data = event['CodePipeline.job']['data']
        params = _get_user_parameters(job_data)
        params_fc = {"high": 1,
                     "medium": 1,
                     "low": 1,
                     "operator": "or"}
        if params:
            if 'FailureCriteria' in params:
                criteria = params['FailureCriteria']
                if 'High' in criteria:
                    params_fc["high"] = int(criteria['High'])
                if 'Medium' in criteria:
                    params_fc["high"] = int(criteria['Medium'])
                if 'Low' in criteria:
                    params_fc["high"] = int(criteria['Low'])
                if 'Operator' in criteria:
                    params_fc["operator"] = criteria['Operator'].lower()

        bucketName, obj = find_artifact(event)
        pipeline_name = obj.split('/')[0]
        action_name = obj.split('/')[1]

        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucketName)
        data = s3.Object(bucketName, obj)
        content_length = data.get()['ContentLength']
        print('content_length', content_length)
        if content_length > 5000000:  # 5MB
            raise Exception("The compressed content file size " +
                            str(content_length) +
                            " exceeds 5MB limit for Lambda function.")

        tmp_repo = dump_zip2tmp_repo(data)
        tmpl_type, tmpl_params, tmpl_tags = repo_util.parse_conf(tmp_repo)
        # Extend tags with input parameters and environment vars
        if params and 'Tags' in params:
            repo_util.extend_tags(tmpl_tags, params['Tags'])
        for te in ('prisma_cloud_tags', 'Tags'):
            if te in os.environ:
                repo_util.extend_tags(tmpl_tags, os.environ[te].split(','))

        asset_type = "AWSCodePipeline"
        tmpl_meta = {}
        tmpl_meta["lambda-name"] = function_name
        tmpl_meta["pipeline-name"] = pipeline_name
        tmpl_meta["job-id"] = job_id
        tmpl_meta["stage-name"] = stage_name
        tmpl_meta["action-name"] = action_name

        logging.info("Initiate template IaC scan ...")
        logging.info("asset name: " + asset_name)
        logging.info("template type: " + tmpl_type)
        logging.info("template meta: " + str(tmpl_meta))
        logging.info("template parameters: " + str(tmpl_params))
        logging.info("template tags: " + str(tmpl_tags))
        logging.info("failure criteria: " + str(params_fc))

        tmpl_zipped, tmpl_zip = create_tmpl_zip(tmp_repo, tmpl_type)
        if tmpl_zipped:
            tmpl_status, tmpl_result = \
                scan_util.scan(pcs, accesskey, secretKey,
                               asset_name,  "AWSCodePipeline",
                               tmpl_zip,
                               tmpl_type, tmpl_params,
                               tmpl_tags, tmpl_meta, params_fc)
        else:
            logging.warning(
                "No files found in the template repo, scan skipped")
            logging.warning("Please check your prisma_cloud_repo_dir variable")
            put_job_success(job_id, "Scan skipped for no template files found")
            return "Complete."

        logging.info("Compose report ...")
        tmpl_summary = tmpl_result["meta"]["matchedPoliciesSummary"]
        tmpl_errors = tmpl_result["meta"]["errorDetails"]
        tmpl_violations = tmpl_result["data"]

        if tmpl_status == "passed":
            if not tmpl_violations:
                put_job_success(job_id, "Success with no issues.")
            else:
                output = output_artifact(event, tmpl_errors, tmpl_violations)
                put_job_success(job_id,
                                """Prisma Cloud IaC scan succeeded with issues as security issues count (High: {0}, Medium: {1}, Low: {2}) below failure criteria (high: {3}, medium: {4}, low: {5}, operator: {6}). {7}"""
                                .format(
                                    tmpl_summary["high"],
                                    tmpl_summary["medium"],
                                    tmpl_summary["low"],
                                    params_fc["high"],
                                    params_fc["medium"],
                                    params_fc["low"],
                                    params_fc["operator"],
                                    "" if not output else "See report at: " + output))

            return "Complete."
        if tmpl_status == "error":
            put_job_failure(job_id, json.dumps(tmpl_errors, indent=2))
            return "Complete."

        output = output_artifact(event, tmpl_errors, tmpl_violations)
        put_job_failure(job_id,
                        """Prisma Cloud IaC scan failed with issues as security issues count (High: {0}, Medium: {1}, Low: {2}) meets or exceeds failure criteria (high: {3}, medium: {4}, low: {5}, operator: {6}). {7}"""
                        .format(
                            tmpl_summary["high"],
                            tmpl_summary["medium"],
                            tmpl_summary["low"],
                            params_fc["high"],
                            params_fc["medium"],
                            params_fc["low"],
                            params_fc["operator"],
                            "" if not output else "See report at: " + output))
    except Exception as e:
        print(e)
        traceback.print_exc()
        put_job_failure(job_id, str(e))
    return "Complete."
